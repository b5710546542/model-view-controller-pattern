# Model-View-Controller Pattern #
## Description ##
### MVC can be used in many different situations. It divides a difficult code into three interconnected parts by their function.  ###
## Components ##
* M is MODEL : directly manages the data, logic and rules of the application.
* V is VIEW :  represent the data to user by the user interface. And user can input information to send to controller.
* C is CONTROLLER : accepts input from view and converts it to commands for the model.

## Communicate of Model-View-Controller  ##
```
User will use a program by view (User Interface). Then, view will send that data to 
controller for management (send to process in model). Finally, the view will be updated from 
the data of model(Model use controller for communicate with view because they don't know 
directly).
```

## Example ##

![mvc.png](https://bitbucket.org/repo/a9Eqgb/images/2912995322-mvc.png)

### Student is model class ###
It's a class that will be call by controller and create the data to be an object.
```
public class Student {
 private int id;
 private String name;
 
 public Student(int id, String name) {
 this.id = id;
 this.name = name;
 }
 
 public int getID() {
 return id;
 }
 
 public void setID(int id) {
 this.id = id;
 }
 
 public String getName() {
 return name;
 }
 
 public void setName(String name) {
 this.name = name;
 }
}
```

### StudentController is controller class ###
It's a class that have a view class for connect with user and send the data to model class.
```
import java.util.ArrayList;
 
public class StudentController {
 private ArrayList<Student> models;
 private StudentView view;
 
 public StudentController(ArrayList<Student> students, StudentView view) {
 this.models = students;
 this.view = view;
 view.setController(this);
 }
 
 public void addNewStudent(int id, String name) {
 models.add(new Student(id, name));
 }
 
 public void removeStudent(int id, String name) {
 models.remove(searchStudent(id, name));
 }
 
 public Student searchStudent(int id, String name) {
 Student s = null;
 for (Student student : models) {
 if (student.getID() == id && student.getName().equals(name)) {
 s = student;
 break;
 }
 }
 return s;
 }
 
 public String getAllStudentDetails() {
 String studentDetails = "";
 for (Student student : models) {
 studentDetails += "ID : " + student.getID() + "\n";
 studentDetails += "Name : " + student.getName() + "\n";
 }
 return studentDetails;
 }
}
```

### StudentView is view interface ###
It's a interface of view that can be implement on another class.
```
public interface StudentView {
 
 public void setController(StudentController studentController);
 
}
```

### StudentControllerView is view class ###
It's a class that will show on console for user can input the data.
```
import java.util.Scanner;
 
public class StudentConsoleView implements StudentView{
 private StudentController studentController;
 
 public void setController(StudentController studentController) {
 this.studentController = studentController;
 }
 
 public void printCommand() {
 System.out.println("Please choose command");
 System.out.println("1.Insert new student");
 System.out.println("2.Remove student");
 System.out.println("3.Show all student");
 System.out.println("-----------------");
 }
 
 public void getUserCommand() {
 Scanner scanner = new Scanner(System.in);
 System.out.print("Enter your command : ");
 int command = scanner.nextInt();
 if (command == 1) {
 System.out.println("User command : 1.Insert new Student");
 System.out.print("Enter student ID : ");
 int id = scanner.nextInt();
 System.out.print("Enter student name : ");
 String name = scanner.next();
 studentController.addNewStudent(id, name);
 System.out.println("Add new student success");
 } else if (command == 2) {
 System.out.println("User command : 2.Remove student");
 System.out.print("Enter student ID : ");
 int id = scanner.nextInt();
 System.out.print("Enter student name : ");
 String name = scanner.next();
 studentController.removeStudent(id, name);
 System.out.println("Remove student success");
 } else if (command == 3) {
 System.out.println("User command : 3.Show all student");
 System.out.println(studentController.getAllStudentDetails());
 }
 
 }
}
```

### StudentGUIView is view class ###
It's a GUI class for user input the data.
```
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.*;
 
public class StudentGUIView implements StudentView{
 private JFrame frame;
 private JTextField addIDTextField;
 private JTextField addNameTextField;
 private JTextField removeIDTextField;
 private JTextField removeNameTextField;
 private JTextArea allStudentTextArea = new JTextArea();
 private StudentController studentController;
 public void setController(StudentController studentController) {
 this.studentController = studentController;
 allStudentTextArea.setText(studentController.getAllStudentDetails());
 }
 
 public StudentGUIView() {
 frame = new JFrame();
 frame.setBounds(100, 100, 683, 489);
 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 frame.getContentPane().setLayout(new GridLayout(0, 3, 0, 0));
 
 JPanel panel = new JPanel();
 frame.getContentPane().add(panel);
 panel.setLayout(new BorderLayout(0, 0));
 
 panel.add(allStudentTextArea);
 
 JLabel allStudentLabel = new JLabel("Show all Student");
 panel.add(allStudentLabel, BorderLayout.NORTH);
 
 JPanel removePanel = new JPanel();
 frame.getContentPane().add(removePanel);
 removePanel.setLayout(null);
 
 JLabel addIDLabel = new JLabel("ID");
 addIDLabel.setBounds(36, 63, 46, 14);
 removePanel.add(addIDLabel);
 
 JLabel addNameLabel = new JLabel("Name");
 addNameLabel.setBounds(36, 99, 46, 14);
 removePanel.add(addNameLabel);
 
 addIDTextField = new JTextField();
 addIDTextField.setBounds(107, 63, 86, 20);
 removePanel.add(addIDTextField);
 addIDTextField.setColumns(10);
 
 addNameTextField = new JTextField();
 addNameTextField.setBounds(107, 96, 86, 20);
 removePanel.add(addNameTextField);
 addNameTextField.setColumns(10);
 
 JButton btnNewButton = new JButton("Add new student");
 btnNewButton.addActionListener(new ActionListener() {
 
 @Override
 public void actionPerformed(ActionEvent e) {
 studentController.addNewStudent(Integer.parseInt(addIDTextField.getText()), addNameTextField.getText());
 allStudentTextArea.setText(studentController.getAllStudentDetails());
 }
 });
 btnNewButton.setBounds(50, 141, 130, 23);
 removePanel.add(btnNewButton);
 
 JPanel panel_2 = new JPanel();
 frame.getContentPane().add(panel_2);
 panel_2.setLayout(null);
 
 JLabel removeIDLabel = new JLabel("ID");
 removeIDLabel.setBounds(41, 66, 46, 14);
 panel_2.add(removeIDLabel);
 
 JLabel removeNameLabel = new JLabel("Name");
 removeNameLabel.setBounds(41, 102, 46, 14);
 panel_2.add(removeNameLabel);
 
 JButton btnNewButton_1 = new JButton("Remove student");
 btnNewButton_1.addActionListener(new ActionListener() {
 public void actionPerformed(ActionEvent e) {
 studentController.removeStudent(Integer.parseInt(removeIDTextField.getText()), removeNameTextField.getText());
 allStudentTextArea.setText(studentController.getAllStudentDetails());
 }
 });
 btnNewButton_1.setBounds(55, 142, 124, 23);
 panel_2.add(btnNewButton_1);
 
 removeIDTextField = new JTextField();
 removeIDTextField.setBounds(113, 63, 86, 20);
 panel_2.add(removeIDTextField);
 removeIDTextField.setColumns(10);
 
 removeNameTextField = new JTextField();
 removeNameTextField.setBounds(113, 99, 86, 20);
 panel_2.add(removeNameTextField);
 removeNameTextField.setColumns(10);
 frame.setVisible(true);
 }
}
```
### Exercise ###
* What is the feature of MVC pattern that is different for another pattern?
* Which pattern is the most similar with MVC?
* Write a booking application for ticket of a zoo by use MVC pattern.


### Member ###
* [Jidapar Jettananurak](https://bitbucket.org/b5710546542)
* [Salilthip Phuklang](https://bitbucket.org/b5710546640)

Reference

* [Software design pattern](http://en.wikipedia.org/wiki/Software_design_pattern)

* [Model-view-controller](http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)

* [mvc-model-view-controller-java/](http://www.inwprogramming.com/java/mvc-model-view-controller-java/)